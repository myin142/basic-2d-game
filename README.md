### Credits

Sprites
 - Player - https://rvros.itch.io/animated-pixel-hero
 - Background - https://kenney.nl/assets/background-elements-redux
 - Platforms/Items - https://kenney.nl/assets/simplified-platformer-pack
