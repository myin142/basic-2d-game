extends "res://scripts/LivingEntity.gd"

var direction = Vector2(-1, 0)
var enemy_node

func _physics_process(delta):
	move_and_slide_with_gravity(direction)
	
	if direction.y != 0:
		direction.y = 0
	
	if is_on_wall() or (is_on_floor() and is_on_edge()):
		direction.x *= -1
		turn_for_motion(enemy_node, direction)

func turn_for_motion(obj, motion):
	var scale = obj.get_scale()
	if motion.x != 0:
		scale.x = -1
	
	obj.set_scale(scale)

func is_on_edge():
	return false
