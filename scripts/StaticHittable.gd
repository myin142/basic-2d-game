extends StaticBody2D

export var hit_points = 5

signal zero_hit_points

func set_hit_points(hp):
	hit_points = hp

func get_hit_points():
	return hit_points

# Need this exact function so that DamageDealer can inflict damage
func hittable_hit(damage, pos):
	if no_hit_points():
		return

	hit_points -= damage
	if no_hit_points():
		emit_signal("zero_hit_points")

func no_hit_points():
	return hit_points == 0
