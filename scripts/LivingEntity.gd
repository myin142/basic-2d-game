extends KinematicBody2D

export var hit_points = 10
export var movement_speed = 80

onready var gravity_vector = ProjectSettings.get_setting("physics/2d/default_gravity_vector")
onready var gravity = ProjectSettings.get_setting("physics/2d/default_gravity") * gravity_vector
onready var floor_vector = gravity_vector * -1

signal hit_point_changed(hp, pos)

var velocity = Vector2()

# motion: Vector2 indicating direction of movement
# gravity_power: increase gravity strength
func move_and_slide_with_gravity(motion, gravity_power = 1):
	var h_motion = Vector2(motion.x, 0)
	
	if h_motion.length() > 0:
		velocity.x = h_motion.x * movement_speed
	else:
		velocity.x = 0
	
	if motion.y != 0:
		velocity.y = motion.y
	
	# default gravity too strong
	velocity += (gravity / 10) * gravity_power
	velocity = move_and_slide(velocity, floor_vector)

func turn_for_motion(obj, motion):
	var scale = obj.get_scale()
	if motion.x < 0:
		scale.x = 1
	elif motion.x > 0:
		scale.x = -1
	
	obj.set_scale(scale)

# Need this exact function so that DamageDealer can inflict damage
func hittable_hit(damage, pos):
	if hit_points == 0:
		return

	hit_points -= damage
	emit_signal("hit_point_changed", hit_points, pos)
