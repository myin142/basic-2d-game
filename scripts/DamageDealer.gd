extends Area2D

export var damage = 1

func set_damage(dmg):
	damage = dmg

func get_damage():
	return damage

func hit_damage(body: Node):
	# is_in_group does not work
	if body.has_method("hittable_hit"):
		body.hittable_hit(damage, global_position)
