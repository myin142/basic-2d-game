extends "res://addons/gut/test.gd"

var PlayerScene = load("res://assets/player/player.tscn")
var state
var scene

func before_each():
	scene = PlayerScene.instance()
	add_child(scene)
	state = scene.get_node("AnimationPlayer")

func test_can_create():
	assert_not_null(state)

# Movement
func test_idle_if_not_moving():
	state.update_animation(Vector2(0, 0))
	assert_eq(state.current_animation, state.IDLE_ANIM)

func test_run_if_moving_horizontal():
	state.update_animation(Vector2(1, 0))
	assert_eq(state.current_animation, state.RUN_ANIM)

func test_jump_if_moving_up():
	state.update_animation(Vector2(0, -1))
	assert_eq(state.current_animation, state.JUMP_ANIM)

func test_fall_if_moving_down():
	state.update_animation(Vector2(0, 1))
	assert_eq(state.current_animation, state.FALL_ANIM)

func test_jump_if_moving_up_and_horizontally():
	state.update_animation(Vector2(1, -1))
	assert_eq(state.current_animation, state.JUMP_ANIM)

func test_jump_if_moving_down_and_horizontally():
	state.update_animation(Vector2(1, 1))
	assert_eq(state.current_animation, state.FALL_ANIM)

# Attacking
func test_attack_if_attacking():
	state.attack()
	state.update_animation(Vector2(0, 0))
	assert_string_starts_with(state.current_animation, state.ATTACK_ANIM_PREFIX)

func test_air_attack_if_in_air():
	state.attack()
	state.update_animation(Vector2(0, -1))
	assert_string_starts_with(state.current_animation, state.AIR_ATTACK_ANIM_PREFIX)

# Cannot test with animation finished
# Maybe in the future
