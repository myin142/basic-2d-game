extends "res://addons/gut/test.gd"

var PlayerScene = load("res://assets/player/player.tscn")
var InputScript = "res://scripts/InputWrapper.gd"
var player

func before_each():
	player = PlayerScene.instance()
	add_child(player)
	
func test_create():
	assert_not_null(player)

# Stubbing not working
#func test_motion():
#	stub(InputScript, 'get_action_strength').to_return(100.0)
#	simulate(player, 1, 1)
#	gut.p(player.velocity)
#	assert_ne(player.velocity.x, 0.0)
