extends "res://scripts/SimpleMovingEnemy.gd"

func _ready():
	enemy_node = $"."
	$AnimationPlayer.play("walk")

func is_on_edge():
	return not $RayCast2D.is_colliding()

func _on_Slime_hit_point_changed(hp, pos):
	if hp == 0:
		queue_free()
	else:
		var hit_direction = global_position.x - pos.x
		direction.x = hit_direction
		direction.y = -300

		$AnimationPlayer.play("damage")

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "damage":
		$AnimationPlayer.play("walk")
		direction = direction.normalized()
