extends "res://scripts/LivingEntity.gd"

export var jump_force = -300

onready var anim = $AnimationPlayer
onready var player = $PlayerSprite

func _physics_process(delta):
	var motion = Vector2(Input.get_action_strength("move_right") - Input.get_action_strength("move_left"), 0)

	if Input.is_action_just_pressed("attack"):
		anim.attack(velocity)
		
	if Input.is_action_just_pressed("jump") and is_on_floor():
		motion.y = jump_force

	if anim.is_attacking() and is_on_floor():
		motion.x = 0
	
	turn_for_motion(player, motion)
	
	move_and_slide_with_gravity(motion)
	
	motion.y = velocity.y
	anim.update_animation(motion)
