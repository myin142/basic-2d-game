extends AnimationPlayer

onready var attack_timer = $AttackTimer

const RUN_ANIM = "run"
const IDLE_ANIM = "idle"
const JUMP_ANIM = "jump"
const FALL_ANIM = "fall"

const AIR_ATTACK_ANIM_PREFIX = "air_attack_"
const MAX_AIR_ATTACK_STATE = 2

const ATTACK_ANIM_PREFIX = "attack_"
const MAX_ATTACK_STATE = 3

var attack_state = 1
var last_attack = ATTACK.GROUND
var attack_type = ATTACK.NONE

func update_animation(motion):
	var anim
	
	# Attack State
	if is_attacking():
		
		# Attacking in air
		if attack_type == ATTACK.AIR:
			var air_attack = AIR_ATTACK_ANIM_PREFIX + str(attack_state)
			anim = air_attack
		
		# Attacking on ground
		elif attack_type == ATTACK.GROUND:
			var ground_attack = ATTACK_ANIM_PREFIX + str(attack_state)
			anim = ground_attack
		
		if anim != null:
			play(anim)
			return
	
	# Motion state
	if motion.y != 0:
		# In 2d y coordinates are inverted
		# negative is up, positive is down
		anim = JUMP_ANIM if motion.y < 0 else FALL_ANIM
	else:
		attack_type = ATTACK.NONE
		anim = IDLE_ANIM if motion.x == 0 else RUN_ANIM
	
	play(anim)

func attack(motion):
	attack_type = ATTACK.AIR if motion.y != 0 else ATTACK.GROUND
	
	if attack_type != last_attack:
		last_attack = attack_type
		attack_state = 1
	
	attack_timer.start()

func is_attacking():
	return attack_type != ATTACK.NONE

func _on_AnimationPlayer_animation_finished(anim_name):
	if is_attacking():
		var max_attack = MAX_AIR_ATTACK_STATE if attack_type == ATTACK.AIR else MAX_ATTACK_STATE
		if attack_state == max_attack:
			attack_state = 1
		elif attack_state < max_attack:
			attack_state += 1

		attack_type = ATTACK.NONE

func _on_AttackTimer_timeout():
	attack_state = 1

enum ATTACK {
	GROUND,
	AIR,
	NONE
}
